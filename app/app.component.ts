import {Component, ViewChild, ElementRef, Renderer} from '@angular/core';
import './rxjs-extensions';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    styleUrls: ['app/app.component.css']
})
export class AppComponent {

    private bokovoemenu: ElementRef;
    private menuView:number = 0;
    private menuWidth: string[] = ['277px','60px'];

    constructor(private renderer: Renderer) {

    }

    public expandMenu(event): void {
        this.renderer.setElementStyle(this.bokovoemenu.nativeElement, 'width', this.menuWidth[this.menuView]);
        this.menuView = this.menuView === 0 ? 1 : 0;
    }

    @ViewChild('Bokovoemenu')
    set setBokovoemenu(_input: ElementRef) {
        if (_input !== undefined) {
            this.bokovoemenu = _input;
        }
    }
}