import {Router} from "@angular/router";
import {Component} from "@angular/core";

@Component({
    selector: 'metamodellist',
    templateUrl: 'app/metamodel/metamodel-list.component.html',
    styleUrls: ['app/metamodel/metamodel-list.component.css']
})
export class MetamodelListComponent {
    constructor(private router: Router) {

    }

}