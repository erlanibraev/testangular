// synchronizers-list.component.ts
import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector:'synchronizerslist',
    templateUrl: 'app/synchronizers/synchronizers-list.component.html',
    styleUrls: ['app/synchronizers/synchronizers-list.component.css']
})
export class SynchronizersListComponent {
    constructor(private router: Router) {

    }

}