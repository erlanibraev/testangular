import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector: 'datasetlist',
    templateUrl: 'app/dataset/dataset-list.component.html',
    styleUrls :['app/dataset/dataset-list.component.css']
})
export class DatasetListComponent {

    constructor(private router: Router) {

    }
}