import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector: 'codingsystemlist',
    templateUrl: 'app/codingsystem/codingsystem-list.component.html',
    styleUrls :['app/codingsystem/codingsystem-list.component.css']
})
export class CodingSystemListComponent {

    constructor(private router: Router) {

    }
}