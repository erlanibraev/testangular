import {Routes, RouterModule} from "@angular/router";
import {AgencyListComponent} from "./agency/agency-list.component";
import {HomeComponent} from "./home/home.component";
import {SourceStockListComponent} from "./sourcestock/sourcestock-list.component";
import {NsComponent} from "./ns/ns.component";
import {SynchronizersListComponent} from "./synchronizers/synchronizers-list.component";
import {MetamodelListComponent} from "./metamodel/metamodel-list.component";
import {CodingSystemListComponent} from "./codingsystem/codingsystem-list.component";
import {DatasetListComponent} from "./dataset/dataset-list.component";

const appRoutes : Routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }, {
        path: 'agencylist',
        component: AgencyListComponent
    }, {
        path: 'home',
        component: HomeComponent
    }, {
        path: 'sourcestocklist',
        component: SourceStockListComponent
    }, {
        path: 'ns',
        component: NsComponent
    }, {
        path: 'synchronizerslist',
        component: SynchronizersListComponent
    }, {
        path: 'metamodellist',
        component: MetamodelListComponent
    }, {
        path: 'codingsystemlist',
        component: CodingSystemListComponent
    }, {
        path: 'datasetlist',
        component: DatasetListComponent
    }
];

export const routing = RouterModule.forRoot(appRoutes);