import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector: 'agencylist',
    templateUrl: 'app/agency/agency-list.component.html',
    styleUrls:['app/agency/agency-list.component.css']
})
export class AgencyListComponent {
    constructor(private router: Router) {

    }
}