import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from "./app.component";
import {AgencyListComponent} from "./agency/agency-list.component";
import {routing} from "./app.routing";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {HomeComponent} from "./home/home.component";
import {SourceStockListComponent} from "./sourcestock/sourcestock-list.component";
import {NsComponent} from "./ns/ns.component";
import {SynchronizersListComponent} from "./synchronizers/synchronizers-list.component";
import {MetamodelListComponent} from "./metamodel/metamodel-list.component";
import {CodingSystemListComponent} from "./codingsystem/codingsystem-list.component";
import {DatasetListComponent} from "./dataset/dataset-list.component";

@NgModule({
    imports: [
        BrowserModule,
        routing,
        FormsModule,
        HttpModule
    ],
    declarations: [
        AppComponent,
        AgencyListComponent,
        HomeComponent,
        SourceStockListComponent,
        NsComponent,
        SynchronizersListComponent,
        MetamodelListComponent,
        CodingSystemListComponent,
        DatasetListComponent
    ],
    bootstrap: [AppComponent]

})
export class AppModule {

}