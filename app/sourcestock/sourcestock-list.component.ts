import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector:'sourcestocklist',
    templateUrl:'app/sourcestock/sourcestock-list.component.html',
    styleUrls:['app/sourcestock/sourcestock-list.component.css']
})
export class SourceStockListComponent {

    constructor(private router: Router) {

    }

}